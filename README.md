# Event Assessment 

An ASP.NET MVC web application for receiving and managing event requests.

## Technologies
* ASP.NET MVC 5
* Bootstrap v4.0.0-beta2
* NPoco

## Running the Application
1. Open the solution in Visual Studio 2017. 
2. Build to restore Nuget packages.
3. Create 3 new files in the same directory as the Web.config file to hold local application settings. 

### web_app_settings.secrets
```
<appSettings>
    <add key="EventApprover" value="[Email address of event approver]" />
</appSettings>
```

### web_db_conn.secrets
```
<connectionStrings>
    <add name="EventAssessmentConnString" connectionString="Data Source=[Enter connection];Initial Catalog=[Enter database name];Integrated Security=True" providerName="[Enter provider ex. System.Data.SqlClient]" />
</connectionStrings>
```

### web_smtp.secrets
```
<system.net>
    <mailSettings>
        <smtp from="[noreply@your.institution]">
            <network host="[Mail host address]" />
        </smtp>
    </mailSettings>
</system.net>
```

## Database
[View Database Schema](EventAssessment/Content/db_schema.jpg)