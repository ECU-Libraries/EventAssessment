﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EventAssessment.App_Start.Startup))]
namespace EventAssessment.App_Start
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}