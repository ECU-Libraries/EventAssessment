﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Configuration;
using System.IO;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Owin.Security.Interop;
using ChunkingCookieManager = Microsoft.Owin.Infrastructure.ChunkingCookieManager;

namespace EventAssessment.App_Start
{
    public static class CentralAuthentication
    {
        public const string ApplicationCookie = "CentralAuthenticationType";
    }

    public partial class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                //  These values did not have to change.
                CookieDomain = ConfigurationManager.AppSettings["CookieDomain"],
                LoginPath = new PathString("/Login"),
                Provider = new CookieAuthenticationProvider(),
                CookieHttpOnly = true,
                ExpireTimeSpan = TimeSpan.FromHours(5),

                //  These values changed
                AuthenticationType = CookieAuthenticationDefaults.AuthenticationType,
                CookieName = ConfigurationManager.AppSettings["CookieName"],

                //  This is new and was added
                TicketDataFormat = new AspNetTicketDataFormat(
                new DataProtectorShim(
                        DataProtectionProvider.Create(new DirectoryInfo(@"c:\central-auth\keys\" + ConfigurationManager.AppSettings["KeyPath"]),
                                (builder) =>
                                {
                                    builder.SetApplicationName(ConfigurationManager.AppSettings["CookieName"]);
                                })
                            .CreateProtector(
                                "Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationMiddleware",
                                "Cookies",
                                "v2"))),

                //  This is new and was added
                CookieManager = new ChunkingCookieManager()
            });
        }
    }
}