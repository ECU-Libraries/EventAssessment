﻿using System;
using NPoco;

namespace EventAssessment.Models
{
    public class ManageViewModel
    {
        public EvtOne EvtOne { get; set; }
        public EvtTwo EvtTwo { get; set; }
        public EvtThree EvtThree { get; set; }
        public EvtFour EvtFour { get; set; }
        public EvtFive EvtFive { get; set; }
    }


    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class Event
    {
        public int id { get; set; }
        public int step { get; set; }

        // EvtOne
        public string title { get; set; }
        public string sponsors { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public bool faculty_staff { get; set; }
        public DateTime start_at { get; set; }
        public DateTime end_at { get; set; }
        public SetupCleanup need_setup { get; set; }
        public SetupCleanup need_cleanup { get; set; }
        public string one_card { get; set; }

        //EvtTwo
        public int est_attendance { get; set; }
        public string target_audience { get; set; }
        public string target_audience_addl { get; set; }
        public int? passport_event { get; set; }

        //EvtThree
        public string description { get; set; }
        public string goals { get; set; }
        public StrategicPriority? priority_id { get; set; }
        public string supports_priority { get; set; }
        public string supports_priority_addl { get; set; }
        public string gone_well { get; set; }
        

        //EvtFour
        public SetupType? setup_id { get; set; }
        public bool? food_drink { get; set; }
        public string provide_food { get; set; }
        public string logistical_needs { get; set; }
        public string other_needs { get; set; }

        //EvtFive
        public bool? marketing_consult { get; set; }
        public bool? digital_marketing { get; set; }
        public bool? print_marketing { get; set; }
        public string other_assistance { get; set; }
        public string marketing_contact { get; set; }
        public bool? university_calendar { get; set; }
        public DateTime? submitted { get; set; }
        public bool? approved { get; set; }
        public DateTime? decision_date { get; set; }


        public int? bops_request { get; set; }

        public bool? assessed { get; set; }
        public DateTime? assessed_date { get; set; }
    }
}