﻿using NPoco;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class EvtThree
    {
        public int id { get; set; }
        public int step { get; set; }

        [Required]
        [Display(Name = "Event Description")]
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [Required]
        [Display(Name = "What are your goals for this event?")]
        [DataType(DataType.MultilineText)]
        public string goals { get; set; }

        [Display(Name = "How does this event support the chosen strategic priority?")]
        public StrategicPriority? priority_id { get; set; }

        [Required]
        [Display(Name = "How does this event support the chosen strategic priority?")]
        [DataType(DataType.MultilineText)]
        public string supports_priority { get; set; }

        [Display(Name = "If additional strategic priorities will be supported, please describe which ones and how.")]
        [DataType(DataType.MultilineText)]
        public string supports_priority_addl { get; set; }

        [Required]
        [Display(Name = "Given the goals above, how will you know if this event has been successful?")]
        [DataType(DataType.MultilineText)]
        public string gone_well { get; set; }

        public static implicit operator EvtThree(Event evt)
        {
            return new EvtThree
            {
                id = evt.id,
                step = evt.step,
                priority_id = evt.priority_id,
                description = evt.description,
                goals = evt.goals,
                supports_priority = evt.supports_priority,
                supports_priority_addl = evt.supports_priority_addl,
                gone_well = evt.gone_well
            };
        }

        public static implicit operator Event(EvtThree vm)
        {
            return new Event
            {
                id = vm.id,
                step = vm.step,
                priority_id = vm.priority_id,
                description = vm.description,
                goals = vm.goals,
                supports_priority = vm.supports_priority,
                supports_priority_addl = vm.supports_priority_addl,
                gone_well = vm.gone_well
            };
        }
    }
}