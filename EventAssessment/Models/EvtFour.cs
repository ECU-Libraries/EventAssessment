﻿using System.ComponentModel.DataAnnotations;
using NPoco;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class EvtFour
    {
        public int id { get; set; }
        public int step { get; set; }

        [Display(Name = "Setup")]
        public SetupType? setup_id { get; set; }

        [Display(Name = "Will you be serving food/beverages?")]
        public bool? food_drink { get; set; }

        [Display(Name = "Who will be providing refreshments?")]
        public string provide_food { get; set; }

        public string provider { get; set; }

        [Ignore]
        public LogisticalNeedList LogisticalNeeds { get; set; }

        [Display(Name = "Logistical Needs")]
        public string logistical_needs { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Additional SetUp Needs")]
        public string other_needs { get; set; }

        public static implicit operator EvtFour(Event evt)
        {
            return new EvtFour
            {
                id = evt.id,
                step = evt.step,
                setup_id = evt.setup_id,
                food_drink = evt.food_drink,
                provide_food = evt.provide_food,
                logistical_needs = evt.logistical_needs,
                other_needs = evt.other_needs
            };
        }

        public static implicit operator Event(EvtFour vm)
        {
            return new Event
            {
                id = vm.id,
                step = vm.step,
                setup_id = vm.setup_id,
                food_drink = vm.food_drink,
                provide_food = vm.provide_food,
                logistical_needs = vm.logistical_needs,
                other_needs = vm.other_needs
            };
        }
    }
}