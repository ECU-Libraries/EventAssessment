﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    public class WhyAttend
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsChecked { get; set; }
    }

    [AtLeastOneWhyAttendProperty(ErrorMessage = "You must choose at least one value")]
    public class WhyAttendList
    {
        public List<WhyAttend> Items { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AtLeastOneWhyAttendProperty : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var targetList = value as WhyAttendList;

            if (targetList == null) return new ValidationResult(FormatErrorMessage(this.ErrorMessage));

            foreach (var property in targetList.Items)
            {
                if (property.IsChecked) return ValidationResult.Success;
            }

            return new ValidationResult(FormatErrorMessage(this.ErrorMessage));
        }
    }
}