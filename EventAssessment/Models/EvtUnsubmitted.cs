﻿using NPoco;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class EvtUnsubmitted
    {
        public int id { get; set; }
        public string title { get; set; }
    }
}