﻿using System;
using NPoco;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class HomeListModel
    {
        public int id { get; set; }
        public int step { get; set; }
        public string title { get; set; }
        
        public DateTime start_at { get; set; }
        public DateTime end_at { get; set; }

        public bool approved { get; set; }
    }
}