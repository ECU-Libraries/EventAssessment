﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NPoco;

namespace EventAssessment.Models
{
    [TableName("tblAssessment")]
    [PrimaryKey("id")]
    public class Assessment
    {
        public int id { get; set; }
        public int event_id { get; set; }

        [Required]
        [Display(Name = "Are you more likely to use the library now that you have attend this event or program?")]
        public bool? more_likely { get; set; }

        [Required]
        [Display(Name = "Did this event or program increase your knowledge about the services, collections, and/or resources the library has to offer?")]
        public bool? increase_knowledge { get; set; }

        [Required]
        [Display(Name = "Would you recommend this event or program to others?")]
        public bool? recommend { get; set; }

        [DataType(DataType.MultilineText)]
        public string library_use { get; set; }

        [Ignore]
        public string library_use_other { get; set; }

        [Ignore]
        public HearAboutEventList HearAboutEvents { get; set; }

        [DataType(DataType.MultilineText)]
        public string how_hear { get; set; }

        [Ignore]
        public string how_hear_more { get; set; }

        [Ignore]
        public WhyAttendList WhyAttends { get; set; }

        [DataType(DataType.MultilineText)]
        public string why_attend { get; set; }

        [Ignore]
        public string why_attend_more { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "What was the most important or helpful part of this event for you?")]
        public string how_helpful { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "How could this event/program be improved?")]
        public string how_improved { get; set; }

        public bool? learn_more { get; set; }

        [DataType(DataType.MultilineText)]
        public string contact { get; set; }

        public string use_responses { get; set; }

        public DateTime submitted { get; set; }
    }

    public class AssessmentEvent : Assessment
    {
        public string title { get; set; }
        public StrategicPriority? priority_id { get; set; }
        public bool? assessed { get; set; }
    }

    public class EventWithAssessments
    {
        public List<Assessment> AssessmentList { get; set; }
        public Event Event { get; set; }
        

    }
}