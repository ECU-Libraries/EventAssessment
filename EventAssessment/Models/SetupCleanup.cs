﻿namespace EventAssessment.Models
{
    public enum SetupCleanup
    {
        None = 1,
        ThirtyMinutes = 2,
        SixtyMinutes = 3,
    }
}