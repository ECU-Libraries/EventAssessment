﻿using Newtonsoft.Json;

namespace EventAssessment.Models
{
    public class AppID
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        public AppID() { }
        public AppID(int value) => ID = value;
    }
}