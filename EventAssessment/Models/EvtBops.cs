﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    public class EvtBops
    {
        public int id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone")]
        public string phone { get; set; }

        [Required]
        [Display(Name = "Request")]
        [DataType(DataType.MultilineText)]
        public string request { get; set; }

        public string title { get; set; }
        public DateTime start_at { get; set; }
        public DateTime end_at { get; set; }
        public SetupCleanup need_setup { get; set; }
        public SetupCleanup need_cleanup { get; set; }
        public int est_attendance { get; set; }
        public bool? food_drink { get; set; }
        public string logistical_needs { get; set; }
        public string other_needs { get; set; }
        public string one_card { get; set; }
    }
}