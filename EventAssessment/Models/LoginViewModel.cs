﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EventAssessment.Models
{
    public class LoginViewModel
    {
        [Required, AllowHtml]
        public string Username { get; set; }

        [Required]
        [AllowHtml]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public int id { get; set; }
    }
}