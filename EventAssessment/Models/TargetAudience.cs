﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    public class TargetAudience
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsChecked { get; set; }
    }

    [AtLeastOneProperty(ErrorMessage = "You must choose at least one value")]
    public class TargetAudienceList
    {
        public List<TargetAudience> Items { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AtLeastOnePropertyAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var targetList = value as TargetAudienceList;

            if (targetList == null) return new ValidationResult(FormatErrorMessage(this.ErrorMessage));

            foreach (var property in targetList.Items)
            {
                if (property.IsChecked) return ValidationResult.Success;
            }

            return new ValidationResult(FormatErrorMessage(this.ErrorMessage));
        }
    }
}