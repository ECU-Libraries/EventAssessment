﻿using System;
using NPoco;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class EvtFive
    {
        public int id { get; set; }
        public int step { get; set; }

        public bool faculty_staff { get; set; }

        [Required]
        [Display(Name = "Have you scheduled consultation with our marketing department?")]
        public bool? marketing_consult { get; set; }

        [Required]
        [Display(Name = "Digital marketing materials designed")]
        public bool? digital_marketing { get; set; }

        [Required]
        [Display(Name = "Print marketing materials designed")]
        public bool? print_marketing { get; set; }

        [Display(Name = "Other assistance needed")]
        [DataType(DataType.MultilineText)]
        public string other_assistance { get; set; }

        [Required]
        [Display(Name = "Who is your marketing contact person? ")]
        [StringLength(150, ErrorMessage = "Marketing Contact cannot be longer than 150 characters.")]
        public string marketing_contact { get; set; }

        [Required]
        [Display(Name = "Would you like us to add this to the University calendar?")]
        public bool? university_calendar { get; set; }

        public DateTime? submitted { get; set; }

        public bool? approved { get; set; }
        public DateTime? decision_date { get; set; }

        public int? bops_request { get; set; }
        
        public bool? assessed { get; set; }
        public DateTime? assessed_date { get; set; }

        public static implicit operator EvtFive(Event evt)
        {
            return new EvtFive
            {
                id = evt.id,
                step = evt.step,
                faculty_staff = evt.faculty_staff,
                marketing_consult = evt.marketing_consult,
                digital_marketing = evt.digital_marketing,
                print_marketing = evt.print_marketing,
                other_assistance = evt.other_assistance,
                marketing_contact = evt.marketing_contact,
                university_calendar = evt.university_calendar,
                submitted = evt.submitted,
                approved = evt.approved,
                decision_date = evt.decision_date,
                bops_request = evt.bops_request,
                assessed = evt.assessed,
                assessed_date = evt.assessed_date
            };
        }

        public static implicit operator Event(EvtFive vm)
        {
            return new Event
            {
                id = vm.id,
                step = vm.step,
                faculty_staff = vm.faculty_staff,
                marketing_consult = vm.marketing_consult,
                digital_marketing = vm.digital_marketing,
                print_marketing = vm.print_marketing,
                other_assistance = vm.other_assistance,
                marketing_contact = vm.marketing_contact,
                university_calendar = vm.university_calendar,
                submitted = vm.submitted,
                approved = vm.approved,
                decision_date = vm.decision_date,
                bops_request = vm.bops_request,
                assessed = vm.assessed,
                assessed_date = vm.assessed_date
            };
        }
    }
}