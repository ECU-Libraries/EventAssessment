﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    public class EvtOne
    {
        public int id { get; set; }
        public int step { get; set; }

        [Required]
        [Display(Name = "Official Event Title")]
        [StringLength(125, ErrorMessage = "Title cannot be longer than 125 characters.")]
        public string title { get; set; }

        [Required]
        [Display(Name = "Sponsoring Department(s)")]
        [StringLength(250, ErrorMessage = "Sponsoring Department(s) cannot be longer than 250 characters.")]
        public string sponsors { get; set; }

        [Required]
        [Display(Name = "Coordinator of Event")]
        [StringLength(75, ErrorMessage = "Coordinator of Event cannot be longer than 75 characters.")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [StringLength(75, ErrorMessage = "Email cannot be longer than 75 characters.")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [StringLength(75, ErrorMessage = "Phone cannot be longer than 75 characters.")]
        public string phone { get; set; }

        [Display(Name = "Are you Academic Library Faculty/Staff?")]
        public bool faculty_staff { get; set; }

        [Display(Name = "Starting at")]
        public DateTime start_at { get; set; }

        [Display(Name = "Ending at")]
        public DateTime end_at { get; set; }

        //[Display(Name = "Setup Time Needed?")]
        //public int need_setup { get; set; }

        [Display(Name = "Setup Time Needed?")]
        public SetupCleanup need_setup { get; set; }


        [Display(Name = "Cleanup Time Needed?")]
        public SetupCleanup need_cleanup { get; set; }

        [Display(Name = "Will the 1Card lock/unlock schedule need to be altered?")]
        public bool one_card_alter { get; set; }

        [Display(Name = "Will the 1Card lock/unlock schedule need to be altered?")]
        [DataType(DataType.MultilineText)]
        public string one_card { get; set; }

        //

        [Required]
        [Display(Name = "Event Start Date")]
        public string startDateString { get; set; }

        [Required]
        [Display(Name = "Event Start Time")]
        public string startTimeString { get; set; }

        [Required]
        [Display(Name = "Event End Date")]
        public string endDateString { get; set; }

        [Required]
        [Display(Name = "Event End Time")]
        public string endTimeString { get; set; }

        public static implicit operator EvtOne(Event evt)
        {
            return new EvtOne
            {
                id = evt.id,
                step = evt.step,
                title = evt.title,
                sponsors = evt.sponsors,
                name = evt.name,
                email = evt.email,
                phone = evt.phone,
                faculty_staff = evt.faculty_staff,
                start_at = evt.start_at,
                end_at = evt.end_at,
                need_setup = evt.need_setup,
                need_cleanup = evt.need_cleanup,
                one_card = evt.one_card
            };
        }

        public static implicit operator Event(EvtOne vm)
        {
            return new Event
            {
                id = vm.id,
                step = vm.step,
                title = vm.title,
                sponsors = vm.sponsors,
                name = vm.name,
                email = vm.email,
                phone = vm.phone,
                faculty_staff = vm.faculty_staff,
                start_at = vm.start_at,
                end_at = vm.end_at,
                need_setup = vm.need_setup,
                need_cleanup = vm.need_cleanup,
                one_card = vm.one_card
            };
        }
    }
}