﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    public class HearAboutEvent
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsChecked { get; set; }
    }

    [AtLeastOneAboutProperty(ErrorMessage = "You must choose at least one value")]
    public class HearAboutEventList
    {
        public List<HearAboutEvent> Items { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AtLeastOneAboutProperty : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var targetList = value as HearAboutEventList;

            if (targetList == null) return new ValidationResult(FormatErrorMessage(this.ErrorMessage));

            foreach (var property in targetList.Items)
            {
                if (property.IsChecked) return ValidationResult.Success;
            }

            return new ValidationResult(FormatErrorMessage(this.ErrorMessage));
        }
    }
}