﻿namespace EventAssessment.Models
{
    public enum SetupType
    {
        Other = 1,
        LecturePresentation = 2,
        Reception = 3,
        LuncheonDinner = 4,
        Meeting = 5,
    }
}