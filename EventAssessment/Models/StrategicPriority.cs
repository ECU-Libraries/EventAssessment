﻿namespace EventAssessment.Models
{
    public enum StrategicPriority
    {
        MaximizeStudentSuccess = 1,
        ImpactResearch = 2,
        ServeThePublic = 3,
        Stewardship = 4
    }
}