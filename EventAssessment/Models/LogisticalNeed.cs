﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    public class LogisticalNeed
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsChecked { get; set; }
    }

    //[AtLeastOneNeedProperty(ErrorMessage = "You must choose at least one value")]
    public class LogisticalNeedList
    {
        public List<LogisticalNeed> Items { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AtLeastOneNeedProperty : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var targetList = value as LogisticalNeedList;

            if (targetList == null) return new ValidationResult(FormatErrorMessage(this.ErrorMessage));

            foreach (var property in targetList.Items)
            {
                if (property.IsChecked) return ValidationResult.Success;
            }

            return new ValidationResult(FormatErrorMessage(this.ErrorMessage));
        }
    }
}