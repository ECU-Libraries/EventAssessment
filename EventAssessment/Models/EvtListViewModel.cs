﻿using System.Collections.Generic;

namespace EventAssessment.Models
{
    public class EvtListViewModel
    {
        public List<EvtItemViewModel> New { get; set; }
        public List<EvtItemViewModel> Upcoming { get; set; }
    }
}