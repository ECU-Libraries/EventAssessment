﻿using System;
using NPoco;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class EvtItemViewModel
    {
        public int id { get; set; }
        public int step { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public DateTime start_at { get; set; }
        public DateTime end_at { get; set; }

        //public List<AssessmentEvent> AssessmentList { get; set; }
        public int assessment_count { get; set; }
    }
}