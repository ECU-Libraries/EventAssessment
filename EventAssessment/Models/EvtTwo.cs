﻿using NPoco;
using System.ComponentModel.DataAnnotations;

namespace EventAssessment.Models
{
    [TableName("tblEvent")]
    [PrimaryKey("id")]
    public class EvtTwo
    {
        public int id { get; set; }
        public int step { get; set; }

        [Required]
        [Display(Name = "Estimated Attendance")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than 0")]
        public int est_attendance { get; set; }

        [Ignore]
        [Display(Name = "Who is your target audience? (select as many as apply)")]
        public TargetAudienceList TargetAudiences { get; set; }

        [Display(Name = "Who is your target audience?")]
        public string target_audience { get; set; }

        public string target_audience_addl { get; set; }

        [Display(Name = "Are you planning for this event to be a Passport Event?")]
        public int? passport_event { get; set; }

        public static implicit operator EvtTwo(Event evt)
        {
            return new EvtTwo
            {
                id = evt.id,
                step = evt.step,
                est_attendance = evt.est_attendance,
                target_audience = evt.target_audience,
                target_audience_addl = evt.target_audience_addl,
                passport_event = evt.passport_event
            };
        }

        public static implicit operator Event(EvtTwo vm)
        {
            return new Event
            {
                id = vm.id,
                step = vm.step,
                est_attendance = vm.est_attendance,
                target_audience = vm.target_audience,
                target_audience_addl = vm.target_audience_addl,
                passport_event = vm.passport_event
            };
        }
    }
}