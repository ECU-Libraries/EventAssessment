﻿using EventAssessment.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace EventAssessment.Services
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string[] _allowedroles;
        public CustomAuthorizeAttribute(params string[] users)
        {
            _allowedroles = users;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorize = false;

            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            //var apps = identity.Claims.Where(x => x.Type == "Apps").Select(y => y.Value).FirstOrDefault();

            //foreach (var role in _allowedroles)
            //{
            //    if (role == "Administrators" && apps.Contains("[141]")) authorize = true;
            //}
            string appsJson = identity.Claims.Where(x => x.Type == "Apps").Select(y => y.Value).FirstOrDefault();
            List<AppID> ids = JsonConvert.DeserializeObject<List<AppID>>(appsJson);
            AppID located = ids.FirstOrDefault(x => x.ID == 141);

            if (located != null) authorize = true;

            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}