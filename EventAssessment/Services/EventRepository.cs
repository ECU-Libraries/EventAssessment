﻿using System;
using System.Collections.Generic;
using EventAssessment.Models;
using NPoco;

namespace EventAssessment.Services
{
    public class EventRepository
    {
        private readonly string _connectionString;

        public EventRepository()
        {
            _connectionString = "EventAssessmentConnString";
        }

        public IDatabase Connection => new Database(_connectionString);

        // Assessment Controller
        public AssessmentEvent GetAssessmentView(int id)
        {
            var sql = "SELECT tblAssessment.*, tblEvent.title, tblEvent.priority_id FROM tblAssessment INNER JOIN tblEvent ON tblEvent.id = tblAssessment.event_id WHERE tblAssessment.id=@0";
            AssessmentEvent evt;
            using (var db = Connection)
            {
                evt = db.SingleOrDefault<AssessmentEvent>(sql, id);
            }

            return evt;
        }

        public void InsertFeedback(AssessmentEvent ae)
        {
            foreach (var target in ae.HearAboutEvents.Items)
            {
                if (target.IsChecked) ae.how_hear += target.Value;
            }
            foreach (var target in ae.WhyAttends.Items)
            {
                if (target.IsChecked) ae.why_attend += target.Value;
            }
            var ass = new Assessment
            {
                event_id = ae.event_id,
                more_likely = ae.more_likely,
                increase_knowledge = ae.increase_knowledge,
                recommend = ae.recommend,
                library_use = ae.library_use,
                how_hear = ae.how_hear,
                why_attend = ae.why_attend,
                how_helpful = ae.how_helpful,
                how_improved = ae.how_improved,
                learn_more = ae.learn_more,
                contact = ae.contact,
                use_responses = ae.use_responses,
                submitted = ae.submitted
            };

            using (var db = Connection)
            {
                db.Insert(ass);
            }
        }

        public AssessmentEvent GetAssessmentEvent(int id)
        {
            var sql = "SELECT title, priority_id, assessed FROM tblEvent WHERE id=@0 AND approved = 1 and assessed IS NULL";
            Event evt;
            using (var db = Connection)
            {
                evt = db.SingleOrDefault<Event>(sql, id);
            }


            var ae = new AssessmentEvent
            {
                event_id = id,
                title = evt.title,
                priority_id = evt.priority_id,
                assessed = evt.assessed
            };
            return ae;
        }

        public List<EvtItemViewModel> GetUnAssessed()
        {
            var sql = "SELECT id, title, name, start_at, end_at, (SELECT COUNT(id) FROM tblAssessment WHERE event_id = tblEvent.id) AS assessment_count FROM tblEvent WHERE approved = 1 AND assessed IS NULL AND end_at < @0";
            var a = DateTime.Now.ToString("MM/dd/yyyy hh:mm");
            using (var db = Connection)
            {
                return db.Fetch<EvtItemViewModel>(sql, a);
            }
        }

        public EventWithAssessments GetAssessmentList(int id)
        {
            var al = new EventWithAssessments();
            using (var db = Connection)
            {
                var results = db.FetchMultiple<Event, Assessment>("SELECT title, priority_id FROM tblEvent WHERE id = @0; SELECT * FROM tblAssessment WHERE event_id = @0 ORDER BY submitted DESC", id);
                
                al.Event = results.Item1[0];
                al.AssessmentList = results.Item2;
            }
            return al;
        }

        // Reporting Controller
        public IEnumerable<Event> GetEventsByDateRange(DateTime start, DateTime end)
        {
            var sql = "SELECT id, email, title, priority_id FROM tblEvent WHERE approved = 1 AND (@0 <= end_at AND DATEADD(dd, DATEDIFF(dd,0,start_at), 0) <= @1)";
            var a = start.ToString("MM/dd/yyyy");
            var b = end.ToString("MM/dd/yyyy");
            using (var db = Connection)
            {
                return db.Fetch<Event>(sql, a, b);
            }
        }

        public Event GetSingleEvent(int id)
        {
            using (var db = Connection)
            {
                return db.SingleById<Event>(id);
            }
        }

        // Manage Controller
        public void SetBopsRequested(int id)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET bops_request=1 WHERE id=@0", id);
            }
        }

        public EvtBops GetBopsRequest(string id)
        {
            var sql = "SELECT id, title, start_at, end_at, need_setup, need_cleanup, est_attendance, food_drink, logistical_needs, other_needs, one_card FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.SingleOrDefault<EvtBops>(sql, id);
            }
        }

        public void AssessEvent(string id)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET assessed=@0, assessed_date=@1 WHERE id=@2", 1, DateTime.Now, id);
            }
        }

        public void ApproveEvent(string id)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET approved=@0, decision_date=@1 WHERE id=@2", 1, DateTime.Now, id);
            }
        }

        public void RejectEvent(string id)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET approved=@0, decision_date=@1 WHERE id=@2", 0, DateTime.Now, id);
            }
        }

        public Event GetManageEvent(int id)
        {
            var sql = "SELECT * FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.SingleOrDefault<Event>(sql, id);
            }
        }

        public EvtListViewModel GetNewAndUpcoming()
        {
            var sql = "SELECT id, title, name, start_at, end_at FROM tblEvent WHERE submitted IS NOT NULL AND decision_date IS NULL; " +
                "SELECT id, title, name, start_at, end_at FROM tblEvent WHERE approved = 1 AND start_at >= @0 ORDER BY start_at";

            using (var db = Connection)
            {
                var results = db.FetchMultiple<EvtItemViewModel, EvtItemViewModel>(sql, DateTime.Now.Date);
                return new EvtListViewModel
                {
                    New = results.Item1,
                    Upcoming = results.Item2
                };
            }
        }

        // Home Controller
        public int CloneEvent(int id)
        {
            var sql = "INSERT INTO tblEvent (step, title, sponsors, name, email, phone, faculty_staff, start_at, end_at, need_setup, " +
                "need_cleanup, description, goals, priority_id, supports_priority, supports_priority_addl, gone_well, est_attendance, target_audience, " +
                "target_audience_addl, passport_event, setup_id, food_drink, provide_food, logistical_needs, other_needs, marketing_consult, digital_marketing, print_marketing, " +
                "other_assistance, marketing_contact, university_calendar) SELECT 4, title, sponsors, name, email, phone, faculty_staff, start_at, " +
                "end_at, need_setup, need_cleanup, description, goals, priority_id, supports_priority, supports_priority_addl, gone_well, est_attendance, target_audience, " +
                "target_audience_addl, passport_event, setup_id, food_drink, provide_food, logistical_needs, other_needs, marketing_consult, digital_marketing, print_marketing, " +
                "other_assistance, marketing_contact, university_calendar FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.Execute(sql, id);
            }
        }

        public List<HomeListModel> GetExistingEvents(string email)
        {
            var sql = "SELECT id, title, start_at, end_at, approved  FROM tblEvent WHERE email = @0 AND submitted IS NOT NULL";

            using (var db = Connection)
            {
                return db.Fetch<HomeListModel>(sql, email);
            }
        }

        public List<EvtUnsubmitted> GetUnsubmittedEvents(string email)
        {
            var sql = "SELECT id, title FROM tblEvent WHERE email = @0 AND submitted IS NULL";

            using (var db = Connection)
            {
                return db.Fetch<EvtUnsubmitted>(sql, email);
            }
        }

        public int InsertEvent(Event evt)
        {
            using (var db = Connection)
            {
                db.Insert(evt);
            }
            return evt.id;
        }

        public EvtOne GetEventStepOne(int id)
        {
            var sql = "SELECT id, step, title, sponsors, name, email, phone, faculty_staff, start_at, end_at, need_setup, need_cleanup, one_card FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.FirstOrDefault<EvtOne>(sql, id);
            }
        }

        public void UpdateStepOne(Event evt)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET title=@0, sponsors=@1, name=@2, email=@3, phone=@4, faculty_staff=@5, start_at=@6, end_at=@7, need_setup=@8, need_cleanup=@9, one_card=@10 WHERE id=@11",
                    evt.title, evt.sponsors, evt.name, evt.email, evt.phone, evt.faculty_staff, evt.start_at, evt.end_at, evt.need_setup, evt.need_cleanup, evt.one_card, evt.id);
            }
        }

        public EvtTwo GetEventStepTwo(int id)
        {
            var sql = "SELECT id, step, est_attendance, target_audience, target_audience_addl, passport_event FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.FirstOrDefault<EvtTwo>(sql, id);
            }
        }

        public void UpdateStepTwo(Event evt)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET step=@0, est_attendance=@1, target_audience=@2, target_audience_addl=@3, passport_event=@4 WHERE id=@5", 
                    evt.step, evt.est_attendance, evt.target_audience, evt.target_audience_addl, evt.passport_event, evt.id);
            }
        }

        public EvtThree GetEventStepThree(int id)
        {
            var sql = "SELECT id, step, description, goals, priority_id, supports_priority, supports_priority_addl, gone_well FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.FirstOrDefault<EvtThree>(sql, id);
            }
            
        }

        public void UpdateStepThree(Event evt)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET step=@0, description=@1, goals=@2, priority_id=@3, supports_priority=@4, supports_priority_addl=@5, gone_well=@6 WHERE id=@7", 
                    evt.step, evt.description, evt.goals, evt.priority_id, evt.supports_priority, evt.supports_priority_addl, evt.gone_well, evt.id);
            }
            
        }

        public EvtFour GetEventStepFour(int id)
        {
            var sql = "SELECT id, step, setup_id, food_drink, provide_food, logistical_needs, other_needs FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.FirstOrDefault<EvtFour>(sql, id);
            }
        }

        public void UpdateStepFour(Event evt)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET step=@0, setup_id=@1, food_drink=@2, provide_food=@3, logistical_needs=@4, other_needs=@5 WHERE id=@6", 
                    evt.step, evt.setup_id, evt.food_drink, evt.provide_food, evt.logistical_needs, evt.other_needs, evt.id);
            }
        }

        public EvtFive GetEventStepFive(int id)
        {
            var sql = "SELECT id, step, faculty_staff, marketing_consult, digital_marketing, print_marketing, other_assistance, marketing_contact, university_calendar FROM tblEvent WHERE id = @0";

            using (var db = Connection)
            {
                return db.FirstOrDefault<EvtFive>(sql, id);
            }
        }

        public void UpdateStepFiveStaff(Event evt)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET step=@0, marketing_consult=@1, digital_marketing=@2, print_marketing=@3, other_assistance=@4, submitted=@5 WHERE id=@6",
                    evt.step, evt.marketing_consult, evt.digital_marketing, evt.print_marketing, evt.other_assistance, evt.submitted, evt.id);
            }
        }

        public void UpdateStepFiveNonStaff(Event evt)
        {
            using (var db = Connection)
            {
                db.Update<Event>(
                    "SET step=@0, marketing_contact=@1, university_calendar=@2, submitted=@3 WHERE id=@4",
                    evt.step, evt.marketing_contact, evt.university_calendar, evt.submitted, evt.id);
            }
        }

        public void DeleteEvent(int id)
        {
            using (var db = Connection)
            {
                db.Delete<Event>(id);
            }
        }
    }
}