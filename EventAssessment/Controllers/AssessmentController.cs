﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using EventAssessment.Models;
using EventAssessment.Services;

namespace EventAssessment.Controllers
{
    public class AssessmentController : BaseController
    {
        [CustomAuthorize("Administrators")]
        public ActionResult Index()
        {
            return View(EventRepo.GetUnAssessed());
        }

        [CustomAuthorize("Administrators")]
        public ActionResult View(int id)
        {
            var vm = EventRepo.GetAssessmentView(id);
            return View(vm);
        }

        [CustomAuthorize("Administrators")]
        public ActionResult List(int id)
        {
            var vm = EventRepo.GetAssessmentList(id);
            return View(vm);
        }

        [AllowAnonymous]
        public ActionResult Feedback(int id)
        {
            var vm = EventRepo.GetAssessmentEvent(id);

            vm.how_hear = vm.how_hear ?? string.Empty;
            vm.HearAboutEvents = new HearAboutEventList();
            vm.HearAboutEvents.Items = new List<HearAboutEvent>
            {
                new HearAboutEvent {Text = "Email", Value = "1", IsChecked = vm.how_hear.Contains("1")},
                new HearAboutEvent {Text = "Social media", Value = "2", IsChecked = vm.how_hear.Contains("2")},
                new HearAboutEvent {Text = "Website", Value = "3", IsChecked = vm.how_hear.Contains("3")},
                new HearAboutEvent {Text = "Class/Instructor", Value = "4", IsChecked = vm.how_hear.Contains("4")},
                new HearAboutEvent {Text = "Friend", Value = "5", IsChecked = vm.how_hear.Contains("5")},
                new HearAboutEvent {Text = "Other (specify below)", Value = "6", IsChecked = vm.how_hear.Contains("6")}
            };

            vm.why_attend = vm.why_attend ?? string.Empty;
            vm.WhyAttends = new WhyAttendList();
            vm.WhyAttends.Items = new List<WhyAttend>
            {
                new WhyAttend {Text = "Class requirement or incentive", Value = "1", IsChecked = vm.why_attend.Contains("1")},
                new WhyAttend {Text = "Interest in topic    ", Value = "2", IsChecked = vm.why_attend.Contains("2")},
                new WhyAttend {Text = "Related to area of study", Value = "3", IsChecked = vm.why_attend.Contains("3")},
                new WhyAttend {Text = "Passport event", Value = "4", IsChecked = vm.why_attend.Contains("4")},
                new WhyAttend {Text = "Other (specify below)", Value = "5", IsChecked = vm.why_attend.Contains("5")}
            };
            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Feedback(AssessmentEvent ae)
        {
            if (ae.library_use == "Other")
            {
                ae.library_use = ae.library_use += ": " + ae.library_use_other;
            }
            ae.submitted = DateTime.Now;

            ae.how_hear = ae.how_hear_more + ae.how_hear;
            ae.why_attend = ae.why_attend_more + ae.why_attend;
            EventRepo.InsertFeedback(ae);
            return RedirectToAction("Success");
        }

        [AllowAnonymous]
        public ActionResult Success()
        {
            return View();
        }
    }
}