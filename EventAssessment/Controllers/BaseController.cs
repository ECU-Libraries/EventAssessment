﻿using System.Web.Mvc;
using EventAssessment.Services;

namespace EventAssessment.Controllers
{
    public class BaseController : Controller
    {
        public EventRepository EventRepo => new EventRepository();
    }
}