﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net.Mail;
using System.Security.Claims;
using System.Web.Mvc;
using EventAssessment.Models;

namespace EventAssessment.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var vm = EventRepo.GetUnsubmittedEvents(identity.FindFirst(ClaimTypes.Email).Value);
            return View(vm);
        }

        public ActionResult List()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var vm = EventRepo.GetExistingEvents(identity.FindFirst(ClaimTypes.Email).Value);
            return View(vm);
        }

        public ActionResult View(int id)
        {
            var vm = EventRepo.GetManageEvent(id);
            var tm = new ManageViewModel
            {
                EvtOne = vm,
                EvtTwo = vm,
                EvtThree = vm,
                EvtFour = vm,
                EvtFive = vm
            };
            return View(tm);
        }

        public ActionResult Clone(int id)
        {
            EventRepo.CloneEvent(id);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult New()
        {
            var identity = (ClaimsIdentity)User.Identity;
            ViewBag.Name = identity.FindFirst(ClaimTypes.GivenName).Value + " " + identity.FindFirst(ClaimTypes.Surname).Value; ;
            ViewBag.Email = identity.FindFirst(ClaimTypes.Email).Value;
            ViewBag.Phone = identity.FindFirst(ClaimTypes.OtherPhone) == null ? "" : identity.FindFirst(ClaimTypes.OtherPhone).Value;
            return View();
        }

        [HttpPost]
        public ActionResult New(EvtOne evt)
        {
            if (ModelState.IsValid)
            {
                evt.step = 1;
                evt.start_at = DateTime.ParseExact(evt.startDateString + " " + evt.startTimeString, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);
                evt.end_at = DateTime.ParseExact(evt.endDateString + " " + evt.endTimeString, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);
                if(evt.one_card_alter == true)
                {
                    if(evt.one_card == null)
                    {
                        evt.one_card =  "1Card schedule needs to be altered, no explanation given.";
                    }
                    else
                    {
                        evt.one_card = $"1Card schedule needs to be altered: {evt.one_card}";
                    }
                }
                else
                {
                    evt.one_card = "1Card schedule does not need to be altered";
                }
                var newid = EventRepo.InsertEvent(evt);
                return RedirectToAction("EditStepTwo", new { id = newid });
            }
            return View();
        }

        public ActionResult EditStepOne(int id)
        {
            var vm = EventRepo.GetEventStepOne(id);
            vm.one_card_alter = vm.one_card.Contains("does not") ? false : true;
            return View(vm);
        }

        [HttpPost]
        public ActionResult EditStepOne(EvtOne evt)
        {
            if (ModelState.IsValid)
            {
                evt.start_at = DateTime.ParseExact(evt.startDateString + " " + evt.startTimeString, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);
                evt.end_at = DateTime.ParseExact(evt.endDateString + " " + evt.endTimeString, "yyyy-MM-dd h:mm tt", CultureInfo.InvariantCulture);
                EventRepo.UpdateStepOne(evt);
                return RedirectToAction("EditStepTwo", new { evt.id });
            }
            return View();
        }

        public ActionResult EditStepTwo(int id)
        {
            var vm = EventRepo.GetEventStepTwo(id);

            vm.passport_event = vm.passport_event ?? 3;
            vm.target_audience = vm.target_audience ?? string.Empty;
            vm.TargetAudiences = new TargetAudienceList();
            vm.TargetAudiences.Items = new List<TargetAudience>
            {
                new TargetAudience {Text = "Undergraduate Students", Value = "1", IsChecked = vm.target_audience.Contains("1")},
                new TargetAudience {Text = "Graduate Students", Value = "2", IsChecked = vm.target_audience.Contains("2")},
                new TargetAudience {Text = "Faculty/Staff", Value = "3", IsChecked = vm.target_audience.Contains("3")},
                new TargetAudience {Text = "Community Members", Value = "4", IsChecked = vm.target_audience.Contains("4")},
                new TargetAudience {Text = "K-12", Value = "5", IsChecked = vm.target_audience.Contains("5")},
                new TargetAudience {Text = "Area Educators", Value = "6", IsChecked = vm.target_audience.Contains("6")},
                new TargetAudience {Text = "Other (describe below)", Value = "7", IsChecked = vm.target_audience.Contains("7")}
            };

            return View(vm);
        }

        [HttpPost]
        public ActionResult EditStepTwo(EvtTwo evt)
        {
            evt.step = evt.step > 3 ? evt.step : 3;
            if (ModelState.IsValid)
            {
                foreach (var target in evt.TargetAudiences.Items)
                {
                    if (target.IsChecked) evt.target_audience += target.Value;
                }

                EventRepo.UpdateStepTwo(evt);
                return RedirectToAction("EditStepThree", new { evt.id });
            }
            return View(evt);
        }

        public ActionResult EditStepThree(int id)
        {
            return View(EventRepo.GetEventStepThree(id));
        }

        [HttpPost]
        public ActionResult EditStepThree(EvtThree evt)
        {
            if (ModelState.IsValid)
            {
                evt.step = evt.step > 2 ? evt.step : 2;
                EventRepo.UpdateStepThree(evt);
                return RedirectToAction("EditStepFour", new { evt.id });
            }
            return View();
            
        }

        public ActionResult EditStepFour(int id)
        {
            var vm = EventRepo.GetEventStepFour(id);

            vm.food_drink = vm.food_drink == true ? vm.food_drink : false;
            //if(vm.provide_food == "Aramark") vm.provider = "Aramark";
            vm.provider = vm.provide_food == "Aramark" ? "Aramark" : "Other";
            vm.logistical_needs = vm.logistical_needs ?? string.Empty;
            vm.LogisticalNeeds = new LogisticalNeedList();
            vm.LogisticalNeeds.Items = new List<LogisticalNeed>
            {
                new LogisticalNeed {Text = "Welcome table", Value = "1", IsChecked = vm.logistical_needs.Contains("1")},
                new LogisticalNeed {Text = "Food/drink tables", Value = "2", IsChecked = vm.logistical_needs.Contains("2")},
                new LogisticalNeed {Text = "Microphone", Value = "3", IsChecked = vm.logistical_needs.Contains("3")},
                new LogisticalNeed {Text = "Powerpoint/other presentation", Value = "4", IsChecked = vm.logistical_needs.Contains("4")},
                new LogisticalNeed {Text = "Movie/film", Value = "5", IsChecked = vm.logistical_needs.Contains("5")},
                new LogisticalNeed {Text = "Other (specify below)", Value = "6", IsChecked = vm.logistical_needs.Contains("6")}
            };

            return View(vm);
        }

        [HttpPost]
        public ActionResult EditStepFour(EvtFour evt)
        {
            evt.step = evt.step > 4 ? evt.step : 4;
            if (ModelState.IsValid)
            {
                if (evt.food_drink == true)
                {
                    if (evt.provider == "Aramark") evt.provide_food = "Aramark";
                }
                else if (evt.food_drink == false)
                {
                    evt.provide_food = null;
                }
                foreach (var target in evt.LogisticalNeeds.Items)
                {
                    if (target.IsChecked) evt.logistical_needs += target.Value;
                }

                EventRepo.UpdateStepFour(evt);
                return RedirectToAction("EditStepFive", new { evt.id });
            }
            return View(evt);
        }

        public ActionResult EditStepFive(int id)
        {
            var vm = EventRepo.GetEventStepFive(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult EditStepFive(EvtFive evt)
        {
            evt.step = evt.step > 5 ? evt.step : 5;
            evt.submitted = DateTime.Now;

            if (evt.faculty_staff)
            {
                ModelState.Remove("marketing_contact");
                ModelState.Remove("university_calendar");
            }
            else
            {
                ModelState.Remove("marketing_consult");
                ModelState.Remove("digital_marketing");
                ModelState.Remove("print_marketing");
                ModelState.Remove("other_assistance");
            }
            if (ModelState.IsValid)
            {
                if (evt.faculty_staff)
                {
                    EventRepo.UpdateStepFiveStaff(evt);
                }
                else
                {
                    EventRepo.UpdateStepFiveNonStaff(evt);
                }

                if (!Request.IsLocal)
                {
                    Event fullEvent = EventRepo.GetSingleEvent(evt.id);
                    var internalMessage = new MailMessage();

                    internalMessage.From = new MailAddress("noreply@ecu.edu");
                    internalMessage.To.Add(new MailAddress(ConfigurationManager.AppSettings["EventApprover"]));
                    internalMessage.Subject = "New Event Request has been Submitted";

                    internalMessage.Body = "Event Title: " + fullEvent.title + Environment.NewLine;
                    internalMessage.Body += "Event Start: " + fullEvent.start_at.ToString("MM/dd/yyyy hh:mm tt") + Environment.NewLine;
                    internalMessage.Body += "Event End: " + fullEvent.end_at.ToString("MM/dd/yyyy hh:mm tt") + Environment.NewLine;
                    internalMessage.Body += "Event Description: " + Environment.NewLine + fullEvent.description + Environment.NewLine + Environment.NewLine;
                    internalMessage.Body += "This event can be reviewed at https://lib.ecu.edu/event/manage" + Environment.NewLine + Environment.NewLine;


                    var responseMessage = new MailMessage();
                    responseMessage.From = new MailAddress("noreply@ecu.edu");
                    responseMessage.To.Add(fullEvent.email);
                    responseMessage.Subject ="Your request has been submitted successfully";

                    responseMessage.Body = "Your request has been submitted successfully.  We will contact you within three business days.";
                    

                    var client = new SmtpClient();
                    client.Send(internalMessage);
                    client.Send(responseMessage);
                }

                return RedirectToAction("Submitted");
            }
            return View(evt);
        }

        public ActionResult Submitted()
        {
            return View();
        }

        public ActionResult Delete(int id)
        {
            EventRepo.DeleteEvent(id);
            return RedirectToAction("Index");
        }
    }
}