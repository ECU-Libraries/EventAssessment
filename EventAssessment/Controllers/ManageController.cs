﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.Mvc;
using EventAssessment.Models;
using EventAssessment.Services;

namespace EventAssessment.Controllers
{
    public class ManageController : BaseController
    {
        [CustomAuthorize("Administrators")]
        public ActionResult Index()
        {
            return View(EventRepo.GetNewAndUpcoming());
        }

        [CustomAuthorize("Administrators")]
        public ActionResult Review(int id)
        {
            var vm = EventRepo.GetManageEvent(id);
            var tm = new ManageViewModel
            {
                EvtOne = vm,
                EvtTwo = vm,
                EvtThree = vm,
                EvtFour = vm,
                EvtFive = vm
            };
            return View(tm);
        }

        [HttpPost]
        [CustomAuthorize("Administrators")]
        public ActionResult Approve(string id, string email, string comment, bool reply = false)
        {
            EventRepo.ApproveEvent(id);

            if (reply && !Request.IsLocal)
            {
                var message = new MailMessage();

                message.From = new MailAddress(ConfigurationManager.AppSettings["EventApprover"]);
                message.To.Add(new MailAddress(email));
                message.CC.Add(new MailAddress(ConfigurationManager.AppSettings["EventApprover"]));
                message.CC.Add(new MailAddress(ConfigurationManager.AppSettings["AdditionalEventApprovalCC"]));
                message.CC.Add(new MailAddress(ConfigurationManager.AppSettings["BuildingOpsFloyd"]));
                message.CC.Add(new MailAddress(ConfigurationManager.AppSettings["BuildingOpsRonnie"]));
                message.CC.Add(new MailAddress("brockl@ecu.edu"));
                message.CC.Add(new MailAddress("clarkmich17@ecu.edu"));
                message.CC.Add(new MailAddress("grimesta18@ecu.edu"));
                message.Subject = "Requested Event Approved";
                message.Body = comment + Environment.NewLine + Environment.NewLine;
                var client = new SmtpClient();
                client.Send(message);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [CustomAuthorize("Administrators")]
        public ActionResult Reject(string id, string email, string comment, bool reply = false)
        {
            EventRepo.RejectEvent(id);

            if (reply && !Request.IsLocal)
            {
                var message = new MailMessage();

                message.From = new MailAddress(ConfigurationManager.AppSettings["EventApprover"]);
                message.To.Add(new MailAddress(email));
                message.CC.Add(new MailAddress(ConfigurationManager.AppSettings["EventApprover"]));
                message.Subject = "Requested Event Rejected";
                message.Body = comment + Environment.NewLine + Environment.NewLine;
                var client = new SmtpClient();
                client.Send(message);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [CustomAuthorize("Administrators")]
        public ActionResult Assess(string id)
        {
            EventRepo.AssessEvent(id);

            return RedirectToAction("Review", "Manage", new { id});
        }

        [CustomAuthorize("Administrators")]
        public ActionResult BopsRequest(string id)
        {
            var vm = EventRepo.GetBopsRequest(id);
            vm.name = "Charlotte Fitz Daniels";
            vm.email = "fitzdanielsc16@ecu.edu";
            vm.phone = "328-0287";
            vm.request += $"Building Ops is being requested for the following event{Environment.NewLine}{Environment.NewLine}";
            vm.request += $"Event Title: {vm.title}{Environment.NewLine}";
            vm.request += $"Start: {vm.start_at}{Environment.NewLine}";
            vm.request += $"End: {vm.end_at}{Environment.NewLine}";
            vm.request += $"1Card schedule: {vm.one_card}{Environment.NewLine}";
            vm.request += $"Setup: {(vm.need_setup == SetupCleanup.SixtyMinutes ? "60 min" : vm.need_setup == SetupCleanup.ThirtyMinutes ? "30 min" : "None")}{Environment.NewLine}";
            vm.request += $"Cleanup: {(vm.need_cleanup == SetupCleanup.SixtyMinutes ? "60 min" : vm.need_cleanup == SetupCleanup.ThirtyMinutes ? "30 min" : "None")}{Environment.NewLine}";
            vm.request += $"Est. Attendance: {vm.est_attendance}{Environment.NewLine}";
            vm.request += $"Food/Drink: {vm.food_drink}{Environment.NewLine}";

            if (vm.logistical_needs != null)
            {
                var logistics = string.Empty;
                foreach (var c in vm.logistical_needs.Trim().ToCharArray())
                {
                    switch (c)
                    {
                        case '1':
                            logistics += "Welcome table; ";
                            break;
                        case '2':
                            logistics += "Food/drink tables; ";
                            break;
                        case '3':
                            logistics += "Microphone; ";
                            break;
                        case '4':
                            logistics += "Powerpoint/other presentation; ";
                            break;
                        case '5':
                            logistics += "Movie/film; ";
                            break;
                        case '6':
                            logistics += "Other: " + vm.other_needs;
                            break;

                    }
                }

                vm.request += $"Logistical Needs: {logistics}{Environment.NewLine}";
            }

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize("Administrators")]
        public ActionResult BopsRequest(EvtBops evt)
        {
            //  Send email to BuildOps
            var message = new MailMessage();

            message.From = new MailAddress(ConfigurationManager.AppSettings["EventApprover"]);
            message.To.Add(new MailAddress(ConfigurationManager.AppSettings["BuildingOpsFloyd"]));
            message.To.Add(new MailAddress(ConfigurationManager.AppSettings["BuildingOpsRonnie"]));
            message.CC.Add(new MailAddress(ConfigurationManager.AppSettings["EventApprover"]));
            message.CC.Add(new MailAddress("brockl@ecu.edu"));
            message.CC.Add(new MailAddress("clarkmich17@ecu.edu"));
            message.CC.Add(new MailAddress("grimesta18@ecu.edu"));
            message.Subject = "New Event Request";

            message.Body = $"{evt.request}{Environment.NewLine}{Environment.NewLine}";
            message.Body += $"Please contact {evt.name} at {evt.email} or {evt.phone} with any questions or concerns";

            var client = new SmtpClient();
            client.Send(message);

            
            // create request in bops system, return id
            // send mail

            // set db flag
            EventRepo.SetBopsRequested(evt.id);
            return RedirectToAction("Review", "Manage", new { evt.id });
        }
    }
}