﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using EventAssessment.Services;
using PagedList;

namespace EventAssessment.Controllers
{
    public class ReportingController : BaseController
    {
        [CustomAuthorize("Administrators")]
        public ActionResult Index(string startdate, string enddate, string sortOrder, int? page)
        {
            if (startdate != null)
            {
                var s = DateTime.ParseExact(startdate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var e = DateTime.ParseExact(enddate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                ViewBag.StartDate = s.ToString("yyyy-MM-dd");
                ViewBag.EndDate = e.ToString("yyyy-MM-dd");
                ViewBag.DisplayStart = s.ToString("d");
                ViewBag.DisplayEnd = e.ToString("d");

                ViewBag.CurrentSort = sortOrder;
                ViewBag.StrategicSortParm = sortOrder == "sp_asc" ? "sp_desc" : "sp_asc";

                var vm = EventRepo.GetEventsByDateRange(s, e);

                switch (sortOrder)
                {
                    case "sp_asc":
                        vm = vm.OrderBy(x => x.priority_id);
                        break;
                    case "sp_desc":
                        vm = vm.OrderByDescending(x => x.priority_id);
                        break;
                    default:
                        vm = vm.OrderByDescending(x => x.id);
                        break;
                }


                var pageSize = 10;
                var pageNumber = (page ?? 1);

                return View(vm.ToPagedList(pageNumber, pageSize));
            }

            return View();
        }
    }
}